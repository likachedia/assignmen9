package com.example.assignment9

import android.content.ContentValues
import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.assignment9.databinding.FragmentLogInBinding
import com.example.assignment9.databinding.FragmentLogOut1Binding
import com.example.assignment9.databinding.FragmentRegister1Binding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Register1.newInstance] factory method to
 * create an instance of this fragment.
 */
class Register1 : Fragment() {
 /**   // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }
**/
    private lateinit var auth: FirebaseAuth
    private var _binding: FragmentRegister1Binding? = null
    private val binding get() = _binding!!
    private lateinit var email:String
    private lateinit var password:String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentRegister1Binding.inflate(inflater, container, false)
        auth = Firebase.auth
        setListener()
        val view = binding.root
        return view

    }



   private fun setListener() {
       binding.next.setOnClickListener {
           createAccount()
           //findNavController().navigate(R.id.action_register12_to_register2)
       }
   }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
    override fun onStart() {
        super.onStart()
        val currentUser = auth.currentUser
        if(currentUser != null){
            //reload();
        }
    }

    private fun reload() {
        TODO("Not yet implemented")
    }

    private fun createAccount() {
        email = binding.email.text.toString()
        password = binding.password.text.toString()
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(requireActivity()) { task ->
                if (task.isSuccessful) {
                    Log.d(ContentValues.TAG, "createUserWithEmail:success")
                    val user = auth.currentUser
                    Toast.makeText(context, "Account created successfully.", Toast.LENGTH_SHORT).show()
                    findNavController().navigate(R.id.action_register12_to_register2)
                } else {
                    Log.w(ContentValues.TAG, "createUserWithEmail:failure", task.exception)
                    Toast.makeText(context, "Create account failed.",
                        Toast.LENGTH_SHORT).show()
                }
            }
    }


    /**   companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Register1.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Register1().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            } */

}